﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPI_Addon_VISIO_lorleveque.Roles;
using System.IO;
using System.Diagnostics;

namespace TPI_Addon_VISIO_lorleveque.Export
{
    ///<summary>
    /// Describe the data that we want to export
    ///</summary>
    struct RoleExported
    {
        public string Name { get; }
        public List<string> Tasks { get; }
        public RoleExported(string name, List<string> tasks)
        {
            Name = name;
            Tasks = tasks;
        }
    }
    ///<summary>
    /// Default settings for the export
    ///</summary>
    static class ExportSettings
    {
        private const string CSVSEPARATOR = ";";
        private const string CSVDELIMITOR = "";
        static public string CSVSeparator { get; } = CSVSEPARATOR;
        static public string CSVDelimitor { get; } = CSVDELIMITOR;
    }
    /// <summary>
    /// Class that describe the settings for the dialog window,
    /// I wanted to use a struct, but a struct can't be static
    /// </summary>
    static class DialogSettings
    {
        private const string TITLE = "Emplacement du fichier exporté";
        private const string DEFAULTEXTENSION = ".csv";
        private const string BASEEXPORTSRC = @"C:\";
        private const string FILTERS = @"CSV files (*.csv)|*.csv|All files (*.*)|*.*";
        private const int FILTERINDEX = 1;

        #region getters
        static public string Title { get; } = TITLE;
        static public string DefaultExtension { get; } = DEFAULTEXTENSION;
        static public string BaseExportSRC { get; } = BASEEXPORTSRC;
        static public string Filters { get; } = FILTERS;
        static public int FilterIndex { get; } = FILTERINDEX;
        #endregion
    }
    class Exporter
    {
        ///<summary
        /// Prepare the export by processing the list of roles
        ///</summary>
        ///<param name="roles">The whole list of role</param>
        ///<returns>A list of RoleExported with every data we want to export</returns>
        public List<RoleExported> PrepareExport(List<Role> roles)
        {
            List<RoleExported> toReturn = new List<RoleExported>();

            foreach (Role role in roles)
            {
                List<string> tasks = new List<string>();
                foreach (RoleShape roleShape in role.Shapes)
                {
                    // if a shape has no text, then it's not a instruction
                    if (roleShape.Text != String.Empty)
                    {
                        tasks.Add(roleShape.Text);
                    }
                }

                toReturn.Add(new RoleExported(role.Name, tasks));
            }

            return toReturn;
        }
        ///<summary>
        /// Ask the user settings to export, and export data
        ///</summary>
        ///<param name="roles">A list of data proceed</param>
        public void Export(List<RoleExported> roles)
        {
            (string filePath, bool result) = AskDestFile();
            if (!result)
                return;

            WriteFile(filePath, CreateCSV(roles));
        }

        #region private methods

        ///<summary>
        /// Create a CSV line per line of a role
        ///</summary>
        ///<param name="roles">List of prepared data</param>
        ///<returns>List of lines that are the CSV lines</returns>
        private List<string> CreateCSV(List<RoleExported> roles)
        {
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!
            // i think i can optimize this
            // it's a terrible way
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!

            List<string> CSVFile = new List<string>();

            for (int index = 0; index < roles.Count; index++)
            {
                CSVFile.Add(String.Format("{0}{1}{2}{3}", ExportSettings.CSVDelimitor, roles[index].Name, ExportSettings.CSVDelimitor, ExportSettings.CSVSeparator));
                foreach (string task in roles[index].Tasks)
                {
                    CSVFile[index] += String.Format("{0}{1}{2}{3}", ExportSettings.CSVDelimitor, task, ExportSettings.CSVDelimitor, ExportSettings.CSVSeparator);
                }
            }
            return CSVFile;
        }

        /// <summary>
        /// Ask the dest file with a SaveFileDialog
        /// </summary>
        /// <returns>Tuple (The filename the user wants to, if the cancel button has been pressed)</returns>
        private (string, bool) AskDestFile()
        {
            SaveFileDialog dialog = new SaveFileDialog();

            // set the default settings to the dialog
            defaultSettingsDialog(dialog);

            // show the dialog and return the result
            (string fileName, bool result) = ShowDialogAndGetText(dialog);
            if (!result)
            {
                // no filename
                return ("", false);
            }
            return (fileName, true);
        }
        ///<summary>
        /// Set the differents basic settings to a dialog
        ///</summary>
        ///<param name="dialog">SaveFileDialog we want to assign basic settings</param>
        public void defaultSettingsDialog(SaveFileDialog dialog)
        {
            dialog.InitialDirectory = DialogSettings.BaseExportSRC;
            dialog.Title = DialogSettings.Title;
            dialog.DefaultExt = DialogSettings.DefaultExtension;
            dialog.Filter = DialogSettings.Filters;
            dialog.FilterIndex = DialogSettings.FilterIndex;
        }
        ///<summary>
        /// Show a dialog
        ///</summary>
        ///<param name="dialog">The dialog we want to show</param>
        ///<returns>The result of the dialog</returns>
        private DialogResult ShowDialog(SaveFileDialog dialog)
        {
            return dialog.ShowDialog();
        }
        ///<summary>
        /// Show a dialog and get the path the user chosed
        ///</summary>
        ///<param name="dialog">SaveFileDialog we want to show</param>
        ///<returns>Tuple (The path the user choose, if the user pressed the cancel button)</returns>
        private (string, bool) ShowDialogAndGetText(SaveFileDialog dialog)
        {
            if (ShowDialog(dialog) == DialogResult.OK)
            {
                return (dialog.FileName, true);
            }
            else
            {
                return ("", false);
            }
        }
        ///<summary>
        /// Write a file line per line in a file
        ///</summary>
        ///<param name="fileName">Path of the file we want to write on</param>
        ///<param name="data">Array of lines</param>
        ///<returns></returns>
        private void WriteFile(string fileName, List<string> data)
        {
            using (StreamWriter file = new StreamWriter(fileName))
            {
                foreach (string line in data)
                {
                    file.WriteLine(line);
                }
            }
        }

        #endregion


        #region statics methods
        /// <summary>
        /// Get the size of a Exporter
        /// https://stackoverflow.com/questions/4956435/get-size-of-struct-in-c-sharp
        /// </summary>
        /// <returns>The size of a Exporter</returns>
        static public int GetSize()
        {
            return System.Runtime.InteropServices.Marshal.SizeOf(typeof(Exporter));
        }
        #endregion
    }
}
