﻿namespace TPI_Addon_VISIO_lorleveque.Engines
{
    static public class InternalIndex
    {
        // This index should be nominative
        // Only one object can have this index
        static ushort Index;
        /// <summary>
        /// Get the internal index
        /// </summary>
        /// <returns>The new index</returns>
        static public ushort GetIndex()
        {
            return ++Index;
        }

        #region statics methods
        /// <summary>
        /// Get the size of a InternalIndex
        /// https://stackoverflow.com/questions/4956435/get-size-of-struct-in-c-sharp
        /// </summary>
        /// <returns>The size of a InternalIndex</returns>
        static public int GetSize()
        {
            return System.Runtime.InteropServices.Marshal.SizeOf(typeof(InternalIndex));
        }
        #endregion
    }
}
