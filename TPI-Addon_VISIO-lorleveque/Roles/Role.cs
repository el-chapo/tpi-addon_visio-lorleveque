﻿// Author : Loris Levêque
// Creation Date : 27.05.2021
// Modification Date : 07.06.2021
// Description : Class "Role" describe all things about a role
//  The class "RoleShape" describe all things about a shape linked to a role
//////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPI_Addon_VISIO_lorleveque.Engines;
using Visio = Microsoft.Office.Interop.Visio;

namespace TPI_Addon_VISIO_lorleveque.Roles
{
    /// <summary>
    /// Class that describe a link between two shapes
    /// </summary>
    public class ShapeLink
    {
        public int UniqueIndex { get; }
        public Visio.Shape Shape { get; }

        #region operators and overrides
        /// <summary>
        /// Operator equals
        /// </summary>
        /// <param name="a">first ShapeLink</param>
        /// <param name="b">second ShapeLink</param>
        /// <returns>if equals or not</returns>
        static public bool operator ==(ShapeLink a, ShapeLink b)
        {
            return a.UniqueIndex == b.UniqueIndex;
        }
        /// <summary>
        /// Operator not equals
        /// </summary>
        /// <param name="a">first ShapeLink</param>
        /// <param name="b">second ShapeLink</param>
        /// <returns>if equals or not</returns>
        static public bool operator !=(ShapeLink a, ShapeLink b)
        {
            return a.UniqueIndex != b.UniqueIndex;
        }
        /// <summary>
        /// Override of Equals
        /// Does basically the same thing of base.Equals
        /// </summary>
        /// <param name="obj">Object to check if equals</param>
        /// <returns>if equals or not</returns>
        public override bool Equals(object obj)
        {
            return obj is ShapeLink link &&
                   UniqueIndex == link.UniqueIndex;
        }
        /// <summary>
        /// Base GetHashCode
        /// It gives me a warning if i don't write it
        /// </summary>
        /// <returns>The hashcode</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        /// <summary>
        /// Write the link index, the source and the dest
        /// </summary>
        /// <returns>The string formatted</returns>
        public override string ToString()
        {
            return String.Format(
                "Link N° {0} with name {1}",
                UniqueIndex,
                Shape.Name
            );
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="uniqueIndex">InternalIndex.GetIndex()</param>
        /// <param name="indexDest">UniqueIndex of the source</param>
        /// <param name="indexSource">UniqueIndex of the destination</param>
        public ShapeLink(Visio.Shape shape)
        {
            UniqueIndex = InternalIndex.GetIndex();
            Shape = shape;
        }

        #region statics methods
        /// <summary>
        /// Get the size of a ShapeLink
        /// https://stackoverflow.com/questions/4956435/get-size-of-struct-in-c-sharp
        /// </summary>
        /// <returns>The size of a ShapeLink</returns>
        static public int GetSize()
        {
            return System.Runtime.InteropServices.Marshal.SizeOf(typeof(ShapeLink));
        }
        #endregion
    }
    /// <summary>
    /// Class that describe a shape attributed to a role
    /// Those shape will be connected between them with a link, which is described in the class ShapeLink
    /// </summary>
    public class RoleShape
    {
        /// <summary>
        /// Name of the Shape
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Text of the Shape,
        /// Can be String.Empty
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Tooltips assigned to the shape,
        /// Can be String.Empty
        /// </summary>
        public string ToolTips { get; set; }
        /// <summary>
        /// Unique internal index
        /// </summary>
        public int UniqueIndex { get; }

        public Visio.Shape Shape { get; }
        /// <summary>
        /// List of the links to the other RoleShape
        /// the links are based to the UniqueIndex
        /// </summary>
        public List<ShapeLink> Links { get; private set; }

        #region operators and overrides
        /// <summary>
        /// Operator equals
        /// Doesn't match the links
        /// </summary>
        /// <param name="a">first RoleShape</param>
        /// <param name="b">second RoleShape</param>
        /// <returns>if equals or not</returns>
        static public bool operator ==(RoleShape a, RoleShape b)
        {
            return
                a.Name == b.Name &&
                a.Text == b.Text &&
                a.UniqueIndex == b.UniqueIndex;
        }
        /// <summary>
        /// Operator not equals
        /// Doesn't match the links
        /// </summary>
        /// <param name="a">first RoleShape</param>
        /// <param name="b">second RoleShape</param>
        /// <returns>if equals or not</returns>
        static public bool operator !=(RoleShape a, RoleShape b)
        {
            return
                a.Name != b.Name ||
                a.Text != b.Text ||
                a.UniqueIndex != b.UniqueIndex;
        }
        /// <summary>
        /// Override of Equals
        /// Match the links
        /// </summary>
        /// <param name="obj">target object</param>
        /// <returns>if equals of not</returns>
        public override bool Equals(object obj)
        {
            return obj is RoleShape shape &&
                   Name == shape.Name &&
                   Text == shape.Text &&
                   UniqueIndex == shape.UniqueIndex;
        }
        /// <summary>
        /// Base Hash Code
        /// </summary>
        /// <returns>The Hash Code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        /// <summary>
        /// Override of ToString
        /// Print the name, the uniqueIndex and the number of links
        /// </summary>
        /// <returns>string formatted</returns>
        public override string ToString()
        {
            return String.Format(
                "{0} with index {1} and {2} links",
                Name,
                UniqueIndex,
                Links.Count
            );
        }

        #endregion

        /// <summary>
        /// Constructor
        /// Set the name and the unique index
        /// </summary>
        /// <param name="uniqueIndex">Internal UniqueIndex</param>
        /// <param name="name">Name of the shape</param>
        public RoleShape(Visio.Shape shape, string name)
        {
            Shape = shape;
            UniqueIndex = InternalIndex.GetIndex();
            Name = name;
            Text = String.Empty;
            ToolTips = String.Empty;
            Links = new List<ShapeLink>();
        }

        /// <summary>
        /// Add a link to another RoleShape
        /// </summary>
        /// <param name="uniqueIndex">targeted RoleShape UniqueIndex</param>
        public void AddLink(Visio.Shape shape)
        {
            Links.Add(
                new ShapeLink(shape)
            );
        }
        /// <summary>
        /// Remove the link to the targeted RoleShape
        /// </summary>
        /// <param name="uniqueIndex">targeted RoleShape UniqueIndex</param>
        public void RemoveLink(Visio.Shape shape)
        {
            Links = Links.Where<ShapeLink>(
                link => link.Shape.ID != shape.ID
            ).ToList();
        }
        /// <summary>
        /// Delete all the links shapes
        /// </summary>
        public void DeleteLinks()
        {
            foreach (ShapeLink link in Links)
            {
                link.Shape.Delete();
            }
        }


        #region statics methods
        /// <summary>
        /// Get the size of a RoleShape
        /// https://stackoverflow.com/questions/4956435/get-size-of-struct-in-c-sharp
        /// </summary>
        /// <returns>The size of a RoleShape</returns>
        static public int GetSize()
        {
            return System.Runtime.InteropServices.Marshal.SizeOf(typeof(RoleShape));
        }
        #endregion
    }
    class Role
    {
        /// <summary>
        /// Name of the Role
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Unique internal index
        /// </summary>
        public int UniqueIndex { get; }
        /// <summary>
        /// Index of the collumn
        /// </summary>
        public byte Index { get; set; }
        /// <summary>
        /// Container assigned to the role
        /// </summary>
        public Visio.Shape Shape { get; }
        /// <summary>
        /// List of the Shapes in the collumn
        /// All the shapes are linked to the role
        /// </summary>
        public List<RoleShape> Shapes { get; private set; }

        #region operators and overrides
        /// <summary>
        /// Operator equals
        /// </summary>
        /// <param name="a">first RoleShape</param>
        /// <param name="b">second RoleShape</param>
        /// <returns>if equals or not</returns>
        static public bool operator ==(Role a, Role b)
        {
            return
                a.Name == b.Name &&
                a.UniqueIndex == b.UniqueIndex &&
                a.Index == b.Index &&
                a.ListShapeEquals(b.Shapes);
        }
        /// <summary>
        /// Operator not equals
        /// </summary>
        /// <param name="a">first RoleShape</param>
        /// <param name="b">second RoleShape</param>
        /// <returns>if equals or not</returns>
        static public bool operator !=(Role a, Role b)
        {
            return
                a.Name != b.Name ||
                a.UniqueIndex != b.UniqueIndex ||
                a.Index != b.Index ||
                !a.ListShapeEquals(b.Shapes);
        }
        /// <summary>
        /// Override of Equals
        /// </summary>
        /// <param name="obj">target object</param>
        /// <returns>if equals of not</returns>
        public override bool Equals(object obj)
        {
            return obj is Role role &&
                   Name == role.Name &&
                   UniqueIndex == role.UniqueIndex &&
                   Index == role.Index &&
                   EqualityComparer<List<RoleShape>>.Default.Equals(Shapes, role.Shapes);
        }
        /// <summary>
        /// Base Hash Code
        /// </summary>
        /// <returns>The Hash Code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        /// <summary>
        /// Override of ToString
        /// Print the name, the uniqueIndex, the position in the visioPage and the number of shapes
        /// </summary>
        /// <returns>string formatted</returns>
        public override string ToString()
        {
            return String.Format(
                "{0} with index {1} in position {2} with {3} items",
                Name,
                UniqueIndex,
                Index,
                Shapes.Count
            );
        }

        #endregion

        /// <summary>
        /// Constructor
        /// Set the name, the unique index and the position as index
        /// </summary>
        /// <param name="name"></param>
        /// <param name="index"></param>
        public Role(Visio.Shape shape, string name, int uniqueIndex, byte index)
        {
            Shape = shape;
            Name = name;
            UniqueIndex = uniqueIndex;
            Index = index;
            Shapes = new List<RoleShape>();
        }

        /// <summary>
        /// Add a shape to the role
        /// </summary>
        public void AddShape(Visio.Shape shape, string name)
        {
            Shapes.Add(new RoleShape(
                shape,
                name
            ));
        }
        /// <summary>
        /// Remove a shape and all the links linked to the shape to the role
        /// </summary>
        public void RemoveShapeAndLinks(int shapeID)
        {
            Shapes.First<RoleShape>(roleShape => roleShape.Shape.ID == shapeID).DeleteLinks();
            RemoveShape(shapeID);
        }
        /// <summary>
        /// Remove a shape to the role
        /// </summary>
        public void RemoveShape(int shapeID)
        {
            Shapes = Shapes.Where<RoleShape>(
                roleShape => roleShape.Shape.ID != shapeID
            ).ToList();
        }
        /// <summary>
        /// Create a link for a shape of the role
        /// </summary>
        /// <param name="source">uniqueIndex of the source</param>
        /// <param name="dest">uniqueIndex of the dest</param>
        public void AddLink(Visio.Shape link, Visio.Shape shape)
        {
            Shapes.First<RoleShape>(
                roleShape => roleShape.Shape.ID == shape.ID
            ).AddLink(link);
        }
        /// <summary>
        /// Remove a link for a shape of the role
        /// </summary>
        /// <param name="uniqueIndex">uniqueIndex of the shape where the link goes</param>
        public void RemoveLink(Visio.Shape link, Visio.Shape shape)
        {
            Shapes.First<RoleShape>(
                roleShape => roleShape.Shape.ID == shape.ID
                ).RemoveLink(link);
        }


        #region private methods
        /// <summary>
        /// Match if two list of RoleShape are equals
        /// </summary>
        /// <param name="target">targeted list of RoleShape</param>
        /// <returns>if equals or not</returns>
        private bool ListShapeEquals(List<RoleShape> target)
        {
            if (Shapes.Count == target.Count)
                return false;

            for (int index = 0; index < target.Count; index++)
            {
                if (target[index] != Shapes[index])
                {
                    return false;
                }
            }

            return true;
        }


        #endregion
    }
}
