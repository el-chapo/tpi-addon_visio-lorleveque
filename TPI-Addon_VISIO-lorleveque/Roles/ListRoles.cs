﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visio = Microsoft.Office.Interop.Visio;
using System.Diagnostics;

namespace TPI_Addon_VISIO_lorleveque.Roles
{
    class ListRoles
    {
        public byte NumberOfRoles { get; private set; } = 0;
        public List<Role> Roles { get; private set; } = new List<Role>();

        public ListRoles()
        {

        }
        /// <summary>
        /// Add a role to the list
        /// </summary>
        /// <param name="shape">The shape created for this role</param>
        /// <param name="name">The name of the role, should be the text of the shape</param>
        /// <param name="uniqueIndex">Internal index</param>
        /// <param name="index">Index in the list</param>
        /// <returns>The role added</returns>
        public Role AddRole(Visio.Shape shape, string name, ushort uniqueIndex, byte index)
        {
            OnCreateShapeSettings(shape, index);
            // Create the new Role on the list
            Roles.Add(new Role(
                shape,
                name,
                uniqueIndex,
                index
            ));
            NumberOfRoles++;
            return Roles.Last();
        }
        /// <summary>
        /// Remove a role from his index
        /// </summary>
        /// <param name="index">index of the role we want to remove</param>
        public void RemoveRole(byte index)
        {
            // This operation is necessary to do not have doubled shape.Name
            // this can be moved from there to before ReOrderRolesIndex()
            // Or you have to change the method ReOrderRolesIndex()
            Roles[index].Shape.Name = "TODELETE";
            Roles.RemoveAt(index);
            NumberOfRoles--;
        }
        /// <summary>
        /// Delete all the shape of a role
        /// </summary>
        /// <param name="index">Index of the role which delete all the containing shapes</param>
        public void RemoveAllShapeOfRole(byte index)
        {
            foreach (RoleShape roleShape in Roles[index].Shapes)
            {
                roleShape.Shape.Delete();
            }
        }
        /// <summary>
        /// Get the shape of the role from his index
        /// Usage of LinQ
        /// </summary>
        /// <param name="index">Index of the role</param>
        /// <returns>The shape of the role, throw an exception if not found</returns>
        public Visio.Shape GetShapeFromIndex(byte index) => 
            Roles.First<Role>(role => role.Index == index).Shape;
        /// <summary>
        /// Get the first shape from the list with the value PinX (ResultIU)
        /// </summary>
        /// <param name="pinX">The value PinX of the shape</param>
        /// <returns>The first Shape found</returns>
        public Visio.Shape GetShapeFromPinX(double pinX) =>
            Roles.First<Role>(role => role.Shape.CellsU["PinX"].ResultIU == pinX).Shape;
        /// <summary>
        /// Get a Role from his index, not the index in the list
        /// </summary>
        /// <param name="index">Index of the wanted wanted</param>
        /// <returns>The first Role found</returns>
        public Role GetRoleByIndex(byte index) =>
            Roles.First<Role>(role => role.Index == index);
        /// <summary>
        /// Get a Role from his linked Shape Name
        /// </summary>
        /// <param name="name">The name of the shape linked with the Role</param>
        /// <returns>The First Role found</returns>
        public Role GetRoleByShapeName(string name) =>
            Roles.First<Role>(role => role.Shape.Name == name);

        /// <summary>
        /// Get a role from and ID of a shape containing by the role
        /// </summary>
        /// <param name="id">The ID of the shape we search</param>
        /// <returns>The role containing this shape with the specified ID</returns>
        /// !!! Must change that, that doesn't handle when no role contains the shape, it throws an exception
        public Role GetRoleByContainingShapeId(int id) =>
            Roles.First<Role>(role =>
                role.Shapes.Where<RoleShape>(
                    shape => shape.Shape.ID == id).Any()
            );

        /// <summary>
        /// Get the RoleShape from his shape.ID
        /// </summary>
        /// <param name="id">The ID of the shape contained by a RoleShape wanted</param>
        /// <returns>The RoleShape containing the wanted shape</returns>
        public RoleShape GetRoleShapeByShapeID(int id) =>
            Roles.First<Role>(role =>
                role.Shapes.Where<RoleShape>(
                    shape => shape.Shape.ID == id).Any()
            ).Shapes.First<RoleShape>(roleShape =>
                roleShape.Shape.ID == id
            );

        /// <summary>
        /// Replace and resize a shape form his index
        /// </summary>
        /// <param name="index">index in the list</param>
        /// <param name="pinX">Position X wanted</param>
        /// <param name="width">Width wanted</param>
        public void ReplaceShapeByIndex(byte index, string pinX, string width)
        {
            Roles[index].Shape.CellsU["PinX"].FormulaForce = pinX;
            Roles[index].Shape.CellsU["Width"].FormulaForce = width;
        }
        /// <summary>
        /// ReOrder all the indexes and the name of the roles
        /// The order in the list is primary : index 1 in list = index 1 page
        /// </summary>
        public void ReOrderRolesIndex()
        {
            for (int index = 0; index < Roles.Count; index++)
            {
                Roles[index].Shape.Name = String.Format("Container{0}", index);
                Roles[index].Index = (byte)index;
            }
        }
        /// <summary>
        /// Add 1 to every index from the given startup and rename them to match with heir indexes
        /// </summary>
        /// <param name="startup">index where to start to add 1</param>
        public void AddOneIndexFromIndex(byte startup)
        {
            for (int index = Roles.Count; index > startup; index--)
            {
                Roles[index - 1].Shape.Name = String.Format("Container{0}", index);
                Roles[index - 1].Index = (byte)index;
            }
        }
        /// <summary>
        /// Add 1 to every index to a range and rename them to match with their indexes
        /// </summary>
        /// <param name="start">index where to start to add 1</param>
        /// <param name="end">index where to stop to add 1</param>
        public void AddOneIndexFromRange(byte start, byte end)
        {
            for (byte index = start; index < end; index++)
            {
                Roles[index].Shape.Name = String.Format("Container{0}", index + 1);
                Roles[index].Index = (byte)(index + 1);
            }
        }
        /// <summary>
        /// Remove 1 to every index to a range and rename them to match with their indexes
        /// </summary>
        /// <param name="start">index where to start to remove 1</param>
        /// <param name="end">index where to stop to remove 1</param>
        public void RemoveOneIndexFromRange(byte start, byte end)
        {
            for (int index = start; index < end; index++)
            {
                Roles[index].Shape.Name = String.Format("Container{0}", index - 1);
                Roles[index].Index = (byte)(index - 1);
            }
        }
        /// <summary>
        /// Re-order the list by their index
        /// Uses LinQ
        /// </summary>
        public void ReOrderRolesListByIndex() =>
            Roles = Roles.OrderBy(
                role => role.Index
            ).ToList();


        #region pivate methods
        /// <summary>
        /// Set basics settings for a shape
        /// </summary>
        /// <param name="shape">Shape created</param>
        /// <param name="index">Index in the list</param>
        private void OnCreateShapeSettings(Visio.Shape shape, byte index)
        {
            // not the right way, we should use VisioPageData
            //SetHeight(shape, "9");
            // P.S. : only for the containers
            //RemoveAutoResize(shape);
            SetName(shape, String.Format("Container{0}", index));
        }
        /// <summary>
        /// Set the height of a shape
        /// </summary>
        /// <param name="shape">Wanted shape</param>
        /// <param name="height">Wanted height</param>
        private void SetHeight(Visio.Shape shape, string height) =>
            shape.CellsU["Height"].FormulaForce = height;
        /// <summary>
        /// Set the name of a shape
        /// </summary>
        /// <param name="shape">Wanted shape</param>
        /// <param name="name">Wanted name</param>
        private void SetName(Visio.Shape shape, string name) =>
            shape.Name = name;
        /// <summary>
        /// Desactivate the automatic resize
        /// Only works for containers
        /// </summary>
        /// <param name="shape">Wanted shape</param>
        private void RemoveAutoResize(Visio.Shape shape) =>
            shape.ContainerProperties.ResizeAsNeeded = 0;

        #endregion

        #region statics methods
        /// <summary>
        /// Get the size of a ListRoles
        /// https://stackoverflow.com/questions/4956435/get-size-of-struct-in-c-sharp
        /// </summary>
        /// <returns>The size of a ListRoles</returns>
        static public int GetSize()
        {
            return System.Runtime.InteropServices.Marshal.SizeOf(typeof(ListRoles));
        }
        #endregion
    }
}
