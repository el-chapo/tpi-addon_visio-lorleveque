﻿// Author : Loris Levêque
// Creation Date : 27.05.2021
// Modification Date : 09.06.2021
// Description : Entry point of the program
////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Visio = Microsoft.Office.Interop.Visio;
using Office = Microsoft.Office.Core;
using TPI_Addon_VISIO_lorleveque.Roles;
using TPI_Addon_VISIO_lorleveque.Export;
using System.Diagnostics;
using TPI_Addon_VISIO_lorleveque.Engines;

namespace TPI_Addon_VISIO_lorleveque
{
    ///<summary>
    /// Class that describe things over the Visio page
    ///</summary>
    public class VisioPageData
    {
        private const double BANNEDZONELEFT = 0.25;
        private const double BANNEDZONERIGHT = 0.25;
        private const int BASENUMBEROFROLES = 4;
        private const double FORMULAPINYBASE = 6.0;
        private const double FORMULAHEIGHTBASE = 9.0;

        #region getters
        public double PageWidth { get; }
        public double PageHeight { get; }
        public double BannedZoneLeft { get; } = BANNEDZONELEFT;
        public double BannedZoneRight { get; } = BANNEDZONERIGHT;
        public int BaseNumberOfRoles { get; } = BASENUMBEROFROLES;
        public double FormulaPinYBase { get; } = FORMULAPINYBASE;
        public double FormulaHeightBase { get; } = FORMULAHEIGHTBASE;
        #endregion

        public VisioPageData(double pageWidth, double pageHeight)
        {
            PageWidth = pageWidth;
            PageHeight = pageHeight;
        }

        #region statics methods
        /// <summary>
        /// Get the size of a VisioPageData
        /// https://stackoverflow.com/questions/4956435/get-size-of-struct-in-c-sharp
        /// </summary>
        /// <returns>The size of a VisioPageData</returns>
        static public int GetSize()
        {
            return System.Runtime.InteropServices.Marshal.SizeOf(typeof(VisioPageData));
        }
        #endregion
    }
    ///<summary>
    /// Static class that contains consts required
    ///</summary>
    static class VisioConsts
    {
        //const string CONTAINERROLESHAPENAME = @"Bande Role MS"; // custom shape
        const string CONTAINERROLESHAPENAME = @"Rectangle";
        const string CONTAINERNAMEBASE = @"Container";
        //const string VSSGABARIFILEPATH = @"C:\Users\lorleveque\Documents\Niveau 2 - Procédure d'Organisation.vss"; // custom gabari
        const string VSSGABARIFILEPATH = @"Basic Shapes.vss";

        #region getters
        /// <summary>
        /// Name of the shape that is used for the roles
        /// </summary>
        static public string ContainerRoleShapeName { get; } = CONTAINERROLESHAPENAME;
        /// <summary>
        /// Name of all the containers (Roles),
        /// Every roles have a name like "Container{number}"
        /// </summary>
        static public string ContainerNameBase { get; } = CONTAINERNAMEBASE;
        /// <summary>
        /// File path of the vss file for the custom gabari
        /// </summary>
        static public string VSSGabariFilePath { get; } = VSSGABARIFILEPATH;
        #endregion
    }

    public partial class TPI_Addon
    {
        VisioPageData visioPageData;

        // basic stencil who can draw about anythings
        Visio.Document visioStencilBasic;
        // stencil designed to draw containers
        // not used in the program
        Visio.Document visioStencilContainer;

        Visio.Page visioPage;

        ListRoles listRoles;
        Exporter exporter;


        #region Events
        /// <summary>
        /// Event when starting the addon
        /// </summary>
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
        }
        /// <summary>
        /// Event when closing the addon
        /// </summary>
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }
        /// <summary>
        /// Event when a shape is added on the page
        /// </summary>
        /// <param name="shape">The shape which's being added</param>
        private void ShapeAdded(Visio.Shape shape)
        {
            Debug.WriteLine(shape.NameU);
            // trigger the containers shapes
            if (shape.NameU == VisioConsts.ContainerRoleShapeName)
            {
                if (listRoles.NumberOfRoles > 200)
                    return;

                // determine the position X from where the shape is
                byte index = DetermineIndexByPinX(shape.CellsU["PinX"].ResultIU);
                if (index != listRoles.NumberOfRoles)
                    listRoles.AddOneIndexFromIndex(index);

                listRoles.AddRole(
                    shape,
                    "New Role",
                    InternalIndex.GetIndex(),
                    index
                );
                shape.CellsU["PinY"].FormulaForce = visioPageData.FormulaPinYBase.ToString();
                shape.CellsU["Height"].FormulaForce = visioPageData.FormulaHeightBase.ToString();
                shape.CellChanged += CellChanged;

                if (index != listRoles.NumberOfRoles)
                    listRoles.ReOrderRolesListByIndex();
                ReplaceShapes();
            }
            // Shape that trigger the export methods
            else if (shape.Name.Contains("Étoile à 5 branches"))
            {
                shape.Delete(); // delete the shape
                Export();
            }
            // Other shapes
            // we obviously can only accept a few shapes to align in an array and check if array contains the shape.Name
            // For this project i allow every shapes to align, so no shapes will not be aligned
            else
            {
                // check if not in a container, if true just do nothing, i don't care about the shape
                // not working and i don't know why, and i don't want to work with absolute values
                #region check if not in container
                if (shape.CellsU["PinX"].ResultIU < visioPageData.BannedZoneLeft && // left side
                    shape.CellsU["PinX"].ResultIU > visioPageData.PageWidth - visioPageData.BannedZoneRight && // right side
                    shape.CellsU["PinY"].ResultIU < visioPageData.FormulaPinYBase && // top side
                    shape.CellsU["PinY"].ResultIU > visioPageData.FormulaHeightBase) // bottom side
                    return;
                #endregion


                byte preIndex = DetermineIndexByPinX(shape.CellsU["PinX"].ResultIU);
                byte index = (byte)(preIndex == 0 ? 0 : preIndex - 1);
                double roleWidth = listRoles.Roles[0].Shape.CellsU["Width"].ResultIU;
                double pinX = visioPageData.BannedZoneLeft + index * roleWidth + roleWidth / 2;

                // those lines can print the position of the shape, you can use it to modify the check upside
                #region Debugs
                //Debug.WriteLine("Shape added to the container n°" + index);
                //Debug.WriteLine("Shape y " + shape.Cells["PinY"].ResultIU);
                //Debug.WriteLine("Shape x " + shape.Cells["PinX"].ResultIU);
                #endregion

                shape.CellsU["PinX"].FormulaForce = pinX.ToString();
                shape.CellChanged += CellChanged;
                shape.TextChanged += RoleShapeTextChanged;

                listRoles.Roles[index].AddShape(
                    shape,
                    "Private shape"
                );
            }
        }
        /// <summary>
        /// Event for when a shape is removed from the page
        /// This event trigger just before the shape get deleted
        /// </summary>
        /// <param name="shape">The shape which's getting removed</param>
        private void BeforeShapeDelete(Visio.Shape shape)
        {
            if (shape.Name.Contains(VisioConsts.ContainerNameBase))
            {
                byte idRole = Convert.ToByte(shape.Name.Substring(9));
                listRoles.RemoveAllShapeOfRole(idRole);
                listRoles.RemoveRole(idRole);
                ReplaceShapes();
            }
            else
            {
                // i wanted to use a nullable varible, but i need c# 8
                // so there is way of a "nullable" variable : if IEnnumerable is empty, variable is null
                // IEnnumerable should have only 1 or null value
                IEnumerable<Role> enumRole = listRoles.Roles.Where<Role>(
                    _role => _role.Shapes.Where<RoleShape>(
                        _roleShape => _roleShape.Shape.ID == shape.ID
                    ).Any()
                );
                // if enum is empty, then the shape isn't handled, so i don't care about it
                if (!enumRole.Any())
                    return;

                // remove all the links linked with the shape
                enumRole.First().RemoveShapeAndLinks(shape.ID);
            }
        }
        /// <summary>
        /// Event when a connector is added to a shape
        /// </summary>
        /// <param name="connect">Object containing the link shape, the shape linked, and some others params</param>
        private void ConnectionsAdded(Visio.Connects connect)
        {
            listRoles.GetRoleByContainingShapeId(connect.ToSheet.ID)
                .AddLink(connect.FromSheet, connect.ToSheet);
        }
        /// <summary>
        /// Event when a connector is deleted from a shape
        /// </summary>
        /// <param name="connect">Object containing the link shape, the shape linked, and some others params</param>
        private void ConnectionsDeleted(Visio.Connects connect)
        {
            listRoles.GetRoleByContainingShapeId(connect.ToSheet.ID).RemoveLink(connect.FromSheet, connect.ToSheet);
        }

        /// <summary>
        /// Event that trigger every time a cell is getting changed in someway
        /// </summary>
        /// <param name="cell">The cell whose value has changed</param>
        private void CellChanged(Visio.Cell cell)
        {
            // if a comment is added, just set it to the RoleShape
            if (cell.Name == "Comment")
            {
                listRoles.GetRoleByContainingShapeId(cell.Shape.ID)
                    .Shapes.First<RoleShape>(
                        roleShape => roleShape.Shape.ID == cell.Shape.ID
                    ).ToolTips = cell.ResultStr[0];
            }

            if (cell.Name != "PinX" && cell.Name != "PinY")
                return;

            if (cell.Shape.Name.Contains(VisioConsts.ContainerNameBase))
            {
                // if is PinY, reset to the default pinY and return
                if (IsPinY(cell))
                    return;

                byte indexSource = Convert.ToByte(cell.Shape.Name.Substring(9));
                byte indexDest = DetermineIndexByPinX(cell.ResultIU);

                if (indexSource == indexDest)
                    return;

                // set the destination of the source index
                listRoles.Roles[indexSource].Index = indexDest;
                cell.Shape.Name = "MOVING";

                // update the range of the index who are gonna move (except the focus container)
                if (indexSource < indexDest)
                    listRoles.RemoveOneIndexFromRange((byte)(indexSource + 1), indexDest);
                else
                    listRoles.AddOneIndexFromRange(indexDest, (byte)(indexSource - 1));

                // re-order the list
                listRoles.ReOrderRolesListByIndex();
                listRoles.ReOrderRolesIndex(); // only used for renaming with the right index

                ReplaceShapes();
            }
        }

        /// <summary>
        /// Set the Text of a RoleShape when the shape.Text of this RoleShape is changed
        /// </summary>
        /// <param name="shape">The shape that text has been changed</param>
        private void RoleShapeTextChanged(Visio.Shape shape)
        {
            listRoles.GetRoleByContainingShapeId(shape.ID)
                .Shapes.First<RoleShape>(
                    roleShape => roleShape.Shape.ID == shape.ID)
                .Text = shape.Text;
        }

        #endregion


        /// <summary>
        /// designer support method - generated by Visio
        /// </summary>
        private void InternalStartup()
        {
            // Create the basic template on which we'll work on
            Application.Documents.Add("");
            // active visio page
            visioPage = Application.ActivePage;

            listRoles = new ListRoles();
            exporter = new Exporter();

            #region stencils declarations
            Visio.Documents visioDocs = Application.Documents;
            // basic stencil who can draw about anythings

            visioStencilBasic = visioDocs.OpenEx(
                VisioConsts.VSSGabariFilePath,
                (short)Microsoft.Office.Interop.Visio.VisOpenSaveArgs.visOpenHidden
            );
            // stencil designed to draw containers
            visioStencilContainer = visioDocs.OpenEx(
                Application.GetBuiltInStencilFile(
                    Visio.VisBuiltInStencilTypes.visBuiltInStencilContainers,
                    Visio.VisMeasurementSystem.visMSMetric),
                (short)Microsoft.Office.Interop.Visio.VisOpenSaveArgs.visOpenHidden
            );
            #endregion

            visioPageData = new VisioPageData(
                visioPage.PageSheet.get_CellsU("PageWidth").ResultIU,
                visioPage.PageSheet.get_CellsU("PageHeight").ResultIU
            );

            CreateBaseContainers();

            #region events declarations

            visioPage.ShapeAdded += ShapeAdded;
            visioPage.BeforeShapeDelete += BeforeShapeDelete;
            visioPage.ConnectionsAdded += ConnectionsAdded;
            visioPage.ConnectionsDeleted += ConnectionsDeleted;

            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);

            #endregion
        }


        #region private methods

        /// <summary>
        /// Replace the shapes containers from their personal index
        /// </summary>
        private void ReplaceShapes()
        {
            // TODO
            // we'll have to make something with the layers, the links with the shapes sometimes are going below the containers
            // docs : https://docs.microsoft.com/en-us/office/vba/api/visio.shape.layer

            double shapeRoleWidth = (visioPageData.PageWidth - visioPageData.BannedZoneLeft - visioPageData.BannedZoneRight) / listRoles.NumberOfRoles;
            for (int indexRole = 0; indexRole < listRoles.NumberOfRoles; indexRole++)
            {
                listRoles.ReplaceShapeByIndex(
                    (byte)indexRole,
                    (CalculatePosXRectangle(indexRole, shapeRoleWidth)).ToString(),
                    shapeRoleWidth.ToString()
                );

                // replace the shapes of the roles (always centered)
                for (int indexShape = 0; indexShape < listRoles.Roles[indexRole].Shapes.Count; indexShape++)
                {
                    double pinX = visioPageData.BannedZoneLeft + indexRole * shapeRoleWidth;
                    listRoles.Roles[indexRole].Shapes[indexShape].Shape.CellsU["PinX"].FormulaForce = (pinX).ToString();
                }
            }
            listRoles.ReOrderRolesIndex();
        }
        /// <summary>
        /// Export the data
        /// </summary>
        private void Export()
        {
            exporter.Export(
                exporter.PrepareExport(
                    listRoles.Roles));
        }
        /// <summary>
        /// Create the base containers, the default template
        /// The number of roles can be changed with the const "BaseNumberOfRoles"
        /// </summary>
        private void CreateBaseContainers()
        {
            double shapeWidth = (visioPageData.PageWidth - visioPageData.BannedZoneLeft - visioPageData.BannedZoneRight) / visioPageData.BaseNumberOfRoles;
            for (int index = 0; index < visioPageData.BaseNumberOfRoles; index++)
            {
                listRoles.AddRole(
                    // Visio item as "rectangle" can be changed to get another item
                    // The name of the shape is his "NameU", you can Debug it just with Debug.WriteLine(shape.NameU); in the ShapeAdded event
                    visioPage.Drop(visioStencilBasic.Masters.get_ItemU(
                        VisioConsts.ContainerRoleShapeName),
                        CalculatePosXRectangle(index, shapeWidth),
                        visioPageData.FormulaPinYBase),
                    String.Format("Role n°{0}", index),
                    InternalIndex.GetIndex(),
                    (byte)index
                );
                listRoles.GetShapeFromIndex((byte)index).CellsU["Width"].FormulaForce = shapeWidth.ToString();
                listRoles.GetShapeFromIndex((byte)index).CellsU["Height"].FormulaForce = visioPageData.FormulaHeightBase.ToString();
                listRoles.GetShapeFromIndex((byte)index).CellChanged += CellChanged;
            }
        }
        /// <summary>
        /// Define the index of where the shape should be based on the position X
        /// If the shape is exactly on the same X position than an other, it will be at left (after)
        /// </summary>
        /// <param name="pinX">The position X of the shape</param>
        /// <returns>The index where it should be</returns>
        private byte DetermineIndexByPinX(double pinX)
        {
            // empty zone on the left of the page
            if (pinX >= 300)
                return 0;
            for (byte index = 0; index < listRoles.NumberOfRoles; index++)
            {
                if (pinX < listRoles.Roles[index].Shape.CellsU["PinX"].ResultIU)
                {
                    return index;
                }
            }
            return listRoles.NumberOfRoles;
        }

        /// <summary>
        /// Check if the cell is a cell PinY,
        /// If yes, it will replace it to the basePinY
        /// </summary>
        /// <param name="cell">cell to check</param>
        /// <returns>If cell Name is PinY</returns>
        private bool IsPinY(Visio.Cell cell)
        {
            if (cell.Name == "PinY")
            {
                cell.ResultIU = visioPageData.FormulaPinYBase;
                return true;
            }
            else
            {
                return false;
            }
        }

        private double CalculatePosXRectangle(int index, double width) =>
            index * width + width / 2 + visioPageData.BannedZoneLeft;

        private double CalculatePosXBlameRole(int index, double width) =>
            index * width + visioPageData.BannedZoneLeft;

        #endregion
    }
}
