﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TPI_Addon_VISIO_lorleveque.Engines.Tests
{
    [TestClass()]
    public class InternalIndexTests
    {
        [TestMethod()]
        public void GetIndexFirstTest()
        {
            for (int index = 1; index < 100; index++)
            {
                Assert.AreEqual(InternalIndex.GetIndex(), index);
            }
        }
        [TestMethod()]
        public void GetIndexSecondTest()
        {
            for (int index = 100; index < 200; index++)
            {
                Assert.AreEqual(InternalIndex.GetIndex(), index);
            }
        }
    }
}