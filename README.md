# README #



## What is this repository for? ##

### Quick summary ###
This is made for my TPI - the end of my learning process
It's an addon for Visio, remake an addon made by the OSSAD
It's basically simple, but there is no documentation from microsoft to how to do this
We only have a documentation in VBA (and i don't know VBA) which is there : https://docs.microsoft.com/en-us/office/vba/api/overview/

The main problems are :
- Internet Explorer won't be supported
And the addon made by OSSAD works only with Internet Explorer (which is kinda stupid i think)
- The addon won't be supported too
So we have to maintain it ourself

<br>

* Version
`0.1.4`

<br>

### How do I get set up? ###

* Summary of set up

The set up isn't really hard
The whole thing is to have every programs and dependencies installed correctly

* Needs

    * Windows 10

    * Visio 2019 (the addon is made for it, so just have it)

    * If you want to build the addon, you'll need VS 2019 (i used community)

        * [Visual Studio 16.9.6](https://visualstudio.microsoft.com/fr/thank-you-downloading-visual-studio/?sku=Community&rel=16&apptype=desktop&tech=dotnetCFV&os=windows) <!-- check the links -->

        * Get the .NET FW 4.7.2

        * Check the offices addon when installing VS

* Configuration

The main configuration for visio is to choose your addon, to enable it you have to display the "developer" pin on the tape
For this you have to right-clic on the top-tape, personnalise and check "developer"
Then en the developer pin, you have the button "COM Componant", with this you can select the addon
If there is no addon, you can import one with the button "import"

* Dependencies

The only dependencies you need is for building the addon, it's the office plug-in for VS

* How to run tests

To run tests, it's the classics units tests
To run them you have to use VS

<br>

### Tests ###

* Choosing tests

The tests are chosen with my experience with the Visio Addons
Some of them will be produced with Units Tests
Some of them will be produced with a real person
Both will be documented in a final rapport

* Writing tests

The units tests will be wrote in the classic way, generated by VS
The tests made with a real person cannot be wrote there, be will be documented in the final rapport

* Validating test

Every tests will be validated if there is <u>ZERO</u> errors/problems <u>and</u> must be checked in two ways
After that, the test result will be validated and documented

### Code Quality ###
* Code review

Some code reviews will be done somewhere in the end of the project (about 5-10 june) and documented
The clean code will be respected as much as i can

* Optimizations

The optimizations will be about 100% (the maximum so)
I plan about 5h of optimizations in the project (5-6%)

